################################################################################
# PORT - asynPort created for the Phytron controller
# ADDR - Address of the axis - right digit represents the index of the axis
#       of a I1AM01 module, the remaining digit(s) on the lef represent the 
#       index of the I1AM01 module
################################################################################
# VMAX - Maximum motor velocity
# VELO - Moving velocity
# VBAS - Minimum velocity
# BDST - Backslash distance
# BVEL - Backslash velocity
# BACC - Backslash acceleration
# MRES - Motor resolution (EGU/STEP)
# ERES - Enconder Resolution (EGU/tick)
# DHLM - Dial High Limit
# DLLM - Dial Low limit


file "$(TOP)/db/Phytron_motor.db"
{
pattern
{  N,        M,        DTYP,          PORT,    ADDR,  DESC,  EGU,   DIR,  VELO,  VBAS, VMAX,  ACCL, BDST,  BVEL,  BACC,        MRES,   ERES, PREC, DHLM,   DLLM,  INIT}
{  1,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m1",   mm,   Pos,   1.0,   1.0,  1.0,   1.0,    0,   1.0,   1.0,  0.00015625,    0.1,    3,    0,      0,  "NO"}
{  2,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m2",  deg,   Pos,   1.0,   1.0,  1.0,   0.5,    0,   1.0,   0.5,  0.00031250,    0.1,    3,    0,  -32.0,  "NO"}
{  3,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m3",  deg,   Pos,   0.1,   0.1,  0.1,   0.5,    0,   0.1,   0.5,  0.00062500,    0.1,    3, 23.2,   19.2,  "NO"}
{  4,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m4",  deg,   Pos,   1.0,   1.0,  1.0,   1.0,    0,   1.0,   1.0,  0.00031250,    0.1,    3,    0,      0,  "NO"}
{  5,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m5",   mm,   Pos,   2.5,   2.5,  2.5,   1.5,    0,   2.5,   1.5,  0.01953120,    0.1,    3,    0,      0,  "NO"}
{  6,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m6",   mm,   Pos,   1.8,   1.8,  1.8,   1.0,    0,   1.8,   1.0,  0.00015625,    0.1,    3,    0,      0,  "NO"}
{  7,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m7",  deg,   Pos,   1.0,   1.0,  1.0,   0.5,    0,   1.0,   0.5,  0.00031250,    0.1,    3,    0,  -65.0,  "NO"}
{  8,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m8",  deg,   Pos,   0.1,   0.1,  0.1,   0.5,    0,   0.1,   0.5,  0.00062500,    0.1,    3,    0,      0,  "NO"}
{  9,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1",  "m9",   mm,   Pos,   1.0,   1.0,  1.0,   1.0,    0,   1.0,   1.0,  0.00500000,    0.1,    3,    0,      0,  "NO"}
{ 10,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1", "m10",   mm,   Pos,   1.0,   1.0,  1.0,   1.0,    0,   1.0,   1.0,  0.00500000,    0.1,    3,    0,      0,  "NO"}
{ 11,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1", "m11",   mm,   Pos,   1.0,   1.0,  1.0,   1.0,    0,   1.0,   1.0,  0.00500000,    0.1,    3,    0,      0,  "NO"}
{ 12,  "m$(N)", "asynMotor", phyMotionPort, "$(N)1", "m12",   mm,   Pos,   0.4,   0.4,  0.4,   0.5,    0,   0.4,   0.5,  0.00100000,    0.1,    3, 79.0,   53.0,  "NO"}
}


################################################################################
# P - MUST MATCH Controller specific name in Phytron_MCM01.db substitution 
#     pattern (below)
# PORT - asynPort created for the Phytron controller
# ADDR - Address of the axis - right digit represents the index of the axis
#       of a I1AM01 module, the remaining digit(s) on the lef represent the 
#       index of the I1AM01 module
# TIMEOUT - asyn timeout
# SCAN - Periodic scan rate of Power stage temperature and Motor temperature
#        records
# INIT - If set to YES, ao records will be initialized with the values defined
#       in the macros that follow INIT. If set to NO, records will not be 
#       initialized - this option is useful if auto save/restore is used
# RUN_CURR - Set Motor current in mA.
################################################################################

file "$(TOP)/db/Phytron_I1AM01.db"
{
pattern
{  N,       M,          PORT,    ADDR,        SCAN, TIMEOUT, INIT, HOMING, MODE, POS_OFFSET, NEG_OFFSET, INIT_TIME, POS_TIME, BOOST, SWITCH_TYP, PWR_STAGE, ENC_TYP, ENC_SFI, ENC_DIR, STOP_CURR, RUN_CURR, BOOST_CURR, CURRENT_DELAY, STEP_RES, PS_MON}
{  1, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          4,         1,       0,       0,       0,     200.0,   1200.0,          0,            20,        3,      1}    
{  2, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          7,         1,       0,       0,       0,     120.0,   1200.0,          0,            20,        3,      1}
{  3, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          4,         1,       0,       0,       0,     110.0,   1200.0,          0,            20,        3,      1}
{  4, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      1,    0,          0,          0,        20,       20,     0,          7,         1,       0,       0,       0,     120.0,   1200.0,          0,            20,        3,      1}
{  5, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          4,         1,       0,       0,       0,    1200.0,   2500.0,          0,            20,        3,      1}
{  6, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          4,         1,       0,       0,       0,     120.0,   1200.0,          0,            20,        3,      1}
{  7, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          7,         1,       0,       0,       0,     120.0,   1200.0,          0,            20,        3,      1}
{  8, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          4,         0,       0,       0,       0,     120.0,   1500.0,          0,            20,        3,      1}
{  9, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          0,         0,       0,       0,       0,         0,      100,          0,            20,        3,      1}
{ 10, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          0,         0,       0,       0,       0,         0,      100,          0,            20,        3,      1}
{ 11, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      0,    1,          0,          0,        20,       20,     0,          0,         0,       0,       0,       0,         0,      100,          0,            20,        3,      1}
{ 12, "m$(N)", phyMotionPort, "$(N)1", "10 second",       3,   NO,      1,    0,          0,          0,        20,       20,     0,          7,         1,       0,       0,       0,         0,      100,          0,            20,        3,      1}
}

################################################################################
# P - Controller specific name
# PORT - asynPort created for the Phytron controller
# ADDR - Arbitrary value between 0 and 255
# TIMEOUT - asyn timeout
################################################################################
file "$(TOP)/db/Phytron_MCM01.db"
{
pattern
{        DTYP,          PORT, ADDR, TIMEOUT}
{ "asynMotor", phyMotionPort,    0,      10}
}

