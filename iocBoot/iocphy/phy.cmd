#!../../bin/linux-x86_64/phy

## You may have to change phy to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/phy.dbd"
phy_registerRecordDeviceDriver pdbbase

drvAsynIPPortConfigure("phymotion","$(DEVIP):22222",0,0,1)

## tests
#drvAsynIPPortConfigure("phymotion","192.168.1.10:22222",0,0,1)
#epicsEnvSet("IOCBL", "XNS")
#epicsEnvSet("IOCDEV", "PHY")

## macros
epicsEnvSet("BL", "$(IOCBL)")
epicsEnvSet("DEV", $(IOCDEV")

phytronCreateController ("phyMotionPort", "phymotion", 100, 250, 500)

phytronCreateAxis("phyMotionPort",  1, 1)
phytronCreateAxis("phyMotionPort",  2, 1)
phytronCreateAxis("phyMotionPort",  3, 1)
phytronCreateAxis("phyMotionPort",  4, 1)
phytronCreateAxis("phyMotionPort",  5, 1)
phytronCreateAxis("phyMotionPort",  6, 1)
phytronCreateAxis("phyMotionPort",  7, 1)
phytronCreateAxis("phyMotionPort",  8, 1)
phytronCreateAxis("phyMotionPort",  9, 1)
phytronCreateAxis("phyMotionPort", 10, 1)
phytronCreateAxis("phyMotionPort", 11, 1)
phytronCreateAxis("phyMotionPort", 12, 1)

## Load record instances
#dbLoadRecords("db/tbl_safety.db","BL=$(IOCBL),DEV=$(IOCDEV)")

cd "${TOP}/iocBoot/${IOC}"

## Load Motor definitions
dbLoadTemplate("motor.substitutions.phytron", "P=$(IOCBL):$(IOCDEV):")

## load BKP records
dbLoadRecords("phybkp.db","BL=$(IOCBL),DEV=$(IOCDEV)")
dbLoadTemplate("phybkp.subs", "BL=$(IOCBL),DEV=$(IOCDEV)")

## Autosave Path and Restore settings
set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## Autosave Monitor settings
create_monitor_set("auto_settings.req", 30, "P=$(IOCBL):$(IOCDEV),BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"

epicsThreadSleep(5.0)

dbpf("$(BL):$(DEV):m1.PROC", "1")
dbpf("$(BL):$(DEV):m2.PROC", "1")
dbpf("$(BL):$(DEV):m3.PROC", "1")
dbpf("$(BL):$(DEV):m4.PROC", "1")
dbpf("$(BL):$(DEV):m5.PROC", "1")
dbpf("$(BL):$(DEV):m6.PROC", "1")
dbpf("$(BL):$(DEV):m7.PROC", "1")
dbpf("$(BL):$(DEV):m8.PROC", "1")
dbpf("$(BL):$(DEV):m9.PROC", "1")
dbpf("$(BL):$(DEV):m10.PROC", "1")
dbpf("$(BL):$(DEV):m11.PROC", "1")
dbpf("$(BL):$(DEV):m12.PROC", "1")
